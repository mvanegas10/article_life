#!/usr/bin/env python
# coding: utf-8

# ## Importer des packages

# In[1]:


import pandas as pd
import numpy as np
import sqlalchemy as sql
import configparser
import datetime
import matplotlib.pyplot as plt
import scipy as sp
import seaborn as sns
from sklearn import metrics
from sklearn import preprocessing 
from sklearn.model_selection import train_test_split, KFold, cross_val_score
from sklearn.compose import make_column_transformer, TransformedTargetRegressor
from sklearn.pipeline import make_pipeline
from sklearn import linear_model
from sklearn.neighbors import KNeighborsRegressor


# ## Charger le fichier de configuration

# In[2]:


months = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dic']
dows = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']


# In[3]:


config = configparser.ConfigParser()
config.read('../config/default.ini')


# ## Création de moteur de connexion à la base de données

# In[4]:


engine = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(config['DATABASE']['username'], config['DATABASE']['password'].replace('_percentage_', '%'), config['DATABASE']['host'], config['DATABASE']['port'], config['DATABASE']['database']))


# ## Charger les données
# Sélection des articles affiché après l'année 2018, publié (published=1).

# In[5]:


df_articles = pd.read_sql_query("""SELECT art.id, art.title, art.primarykeyword as keyword, art.category, links.links, CAST(TO_CHAR(art.displaydate,'d') AS INTEGER) AS dow_display, EXTRACT(hour FROM art.displaydate) AS hour_display, EXTRACT(month FROM art.displaydate) AS month_display, CAST(EXTRACT(year FROM art.displaydate) AS VARCHAR) AS year_display, art.source, art.feed, conso.count as conso, chars.words, chars.characters, art.amountvideo AS videos, art.amountaudio AS audios, art.signature, art.title_url, keywords.nb_keywords FROM article_article art INNER JOIN article_nbchar.article_nbchar chars ON art.id = chars.mediaid INNER JOIN (SELECT article_id, SUM(count) AS count FROM egos_conso_article_per_day cs INNER JOIN article_article ar ON cs.article_id = ar.id WHERE cs.date >= ar.displaydate AND cs.date <= dateadd(day, 1, ar.displaydate) GROUP BY article_id) conso ON art.id = conso.article_id LEFT OUTER JOIN (SELECT articleid, COUNT(body) AS links FROM article_paragraph WHERE body LIKE '%%<a%%' GROUP BY articleid) links ON art.id = links.articleid LEFT OUTER JOIN (SELECT articleid, COUNT(keywordid) AS nb_keywords FROM article_article_keyword GROUP BY articleid) keywords ON art.id = keywords.articleid WHERE art.published='1' AND EXTRACT(year FROM art.displaydate) IN ('2018', '2019')""", engine)
df_articles.head()


# ## Exploration des données

# In[6]:


df_articles.shape


# In[7]:


df_articles.columns


# In[8]:


df_articles.head(10)


# In[9]:


df_articles.describe()


# In[10]:


df_articles.isnull().any()


# In[11]:


df_articles['weekday'] = df_articles['dow_display'].apply(lambda x: 'week' if x > 1 and x < 7 else 'weekend')
#df_articles['month_display'] = df_articles['month_display'].apply(lambda x: months[x-1])
#df_articles['dow_display'] = df_articles['dow_display'].apply(lambda x: dows[x-1])
df_articles['url_title'] = df_articles['title_url'].apply(lambda x: x.split(':')[0] if ':' in x else '')
df_articles['depeche'] = df_articles['signature'].apply(lambda x: 1 if True in [w.lower().strip() in ['belga', 'afp'] for w in x.split(' ')] else 0)
df_articles['videos'] = df_articles['videos'].apply(lambda x: 0 if np.isnan(x) else 1)
df_articles['audios'] = df_articles['audios'].apply(lambda x: 0 if np.isnan(x) else 1)
df_articles['title_length'] = df_articles['title'].apply(lambda x: len(x))
df_articles['links'] = df_articles['links'].apply(lambda x: x if x == x else 0)
df_articles['nb_keywords'] = df_articles['nb_keywords'].apply(lambda x: x if x == x else 0)
#df_articles['links'] = df_articles['links'].apply(lambda x: 1 if x > 0 else 0)


# ## Identification des variables

# In[12]:


ID_col = ['id']
target_col = ['conso']
cat_cols = ['category', 'source', 'feed', 'keyword', 'signature', 'depeche', 'url_title', 'weekday']
ord_cols = ['words', 'characters', 'month_display', 'videos', 'audios', 'dow_display', 'title_length', 'hour_display', 'links', 'nb_keywords']
other_cols = []
features = cat_cols.copy()
features.extend(ord_cols)
features.extend(other_cols)


# In[13]:


cat_lists = []
for cat in cat_cols:
    cat_lists.append(df_articles[cat].unique())
cat_lists


# # Transformation des données

# ## Traiter les valeurs manquantes

# ### 1. Supprimer les enregistrements sans informations de consommation

# In[14]:


df_articles = df_articles[~df_articles['conso'].isna()]
df_articles.shape


# ### 2. Supprimer les enregistrements sans informations de nombre des mots

# In[15]:


df_articles = df_articles[~df_articles['characters'].isna()]
df_articles = df_articles[~df_articles['words'].isna()]
df_articles.shape


# In[16]:


df_articles = df_articles[~df_articles['keyword'].isna()]
df_articles.shape


# In[17]:


df_articles.isnull().any()


# In[18]:


df_articles.describe()


# ## Définition de preprocessing

# In[19]:


ord_transformer = preprocessing.PowerTransformer()
scaler_transformer = preprocessing.MinMaxScaler()
cat_transformer = preprocessing.OneHotEncoder(categories=cat_lists)
preprocessor = make_column_transformer(
    (ord_transformer, ord_cols),
    (scaler_transformer, other_cols),
    (cat_transformer, cat_cols)
)


# ## Séparation de données: test et de train

# In[52]:


y = df_articles.conso
x = df_articles[features]


# In[53]:


x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25, random_state=1)


# ## Prédict

# In[54]:


model = make_pipeline(
    preprocessor,
    TransformedTargetRegressor(
        regressor=linear_model.HuberRegressor(epsilon=1, max_iter=500000),#.Ridge(alpha=1e-10), #KNeighborsRegressor(),
        func=np.log10,
        inverse_func=sp.special.exp10
    )  
)


# In[ ]:


cv = KFold(n_splits=10, shuffle=True, random_state=1)
scores = cross_val_score(model, x, y, scoring='neg_mean_absolute_error', cv=cv, n_jobs=-1)
scores = np.absolute(scores)
s_mean = np.mean(scores)
print('Mean MAE: %.3f' % (s_mean))


# In[22]:


pd.DataFrame({'pred': y_pred, 'test': y_test}).plot.scatter(x='pred', y='test')


# In[ ]:


print('Mean Absolute Error', metrics.mean_absolute_error(y_test, y_pred))
print('Mean Squared Error', metrics.mean_squared_error(y_test, y_pred))
print('Root Mean Squared Error', np.sqrt(metrics.mean_squared_error(y_test, y_pred)))


# In[ ]:


results = x_test.copy()
results['y'] = y_test
results['y_pred'] = y_pred
results.to_csv('{}_results.csv'.format(datetime.datetime.strftime(datetime.datetime.now(), '%y%m%d')))


# In[20]:


_ = sns.pairplot(df_articles[['conso', *other_cols, *ord_cols]], kind='scatter', diag_kind='kde')


# In[21]:


tmp = pd.DataFrame(ord_transformer.fit_transform(df_articles[['conso', *ord_cols]]), columns=['conso', *ord_cols])
for col in other_cols:
    tmp[col] = scaler_transformer.fit_transform(df_articles[[col]])
_ = sns.pairplot(tmp, kind='scatter', diag_kind='kde')


# In[ ]:


feature_names = (model.named_steps['columntransformer']
                      .named_transformers_['onehotencoder']
                      .get_feature_names(input_features=cat_cols))
feature_names = np.concatenate(
    [feature_names, ord_cols, other_cols])

coefs = pd.DataFrame(
    model.named_steps['transformedtargetregressor'].regressor_.coef_,
    columns=['Coefficients'], index=feature_names
)

coefs


# In[ ]:


coefs['abs_coef'] = coefs['Coefficients'].apply(lambda x: abs(x))


# In[ ]:


coefs[coefs['abs_coef'] > 0.6][['Coefficients']].plot(kind='barh', figsize=(9, 7))
plt.title('Ridge model, small regularization')
plt.axvline(x=0, color='.5')
plt.subplots_adjust(left=.3)


# In[ ]:


pd.read_sql_query("""SELECT * from egos_conso_article_connected_user limit 1""", engine).columns


# In[ ]:


df_network = pd.read_sql_query("""SELECT detail.uid, art.id AS art_id, art.feed, EXTRACT(month FROM detail.date) AS month, EXTRACT(year FROM detail.date) AS year, COUNT(detail.date) FROM egos_conso_article_connected_user detail INNER JOIN article_article art ON art.id = detail.article_id WHERE date >= '2019-07-01'""", engine)
df_network.head()


# In[ ]:


df_nod_art =  

